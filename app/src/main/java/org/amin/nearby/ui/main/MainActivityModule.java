package org.amin.nearby.ui.main;


import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;

import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.Module;
import dagger.Provides;

@Module
public class MainActivityModule {

    @Provides
    VenueAdapter provideVenueAdapter() {
        return new VenueAdapter(new ArrayList<>());
    }

    @Provides
    LinearLayoutManager provideLinearLayoutManager(MainActivity mainActivity) {
        return new LinearLayoutManager(mainActivity);
    }

    @Provides
    GoogleApiClient providesGoogleApi(MainActivity mainActivity) {
        return new GoogleApiClient.Builder(mainActivity)
                .addOnConnectionFailedListener(mainActivity)
                .addConnectionCallbacks(mainActivity)
                .addApi(LocationServices.API)
                .build();
    }

    @Provides
    LocationCallback provideLocationCallBack(final MainActivity mainActivity) {
        return new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                mainActivity.getViewModel().setLocation(locationResult.getLastLocation());
            }

            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                super.onLocationAvailability(locationAvailability);
                if (!locationAvailability.isLocationAvailable()) {
                    mainActivity.handleError(new Throwable("Turn your Location Sensor on and try again"));
                }
            }
        };
    }

    @Provides
    LocationRequest provideLocationRequest() {
        return new LocationRequest().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


}
