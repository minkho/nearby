package org.amin.nearby.ui.main;

import android.Manifest;
import android.annotation.SuppressLint;
import android.location.Location;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.amin.nearby.BR;
import org.amin.nearby.R;
import org.amin.nearby.ViewModelProviderFactory;
import org.amin.nearby.data.models.db.Venue;
import org.amin.nearby.databinding.ActivityMainBinding;
import org.amin.nearby.ui.base.BaseActivity;
import org.amin.nearby.ui.main.detail.DetailFragment;
import org.amin.nearby.utils.Constants;
import org.amin.nearby.utils.InfiniteScrollListener;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class MainActivity extends BaseActivity<ActivityMainBinding, MainViewModel> implements HasSupportFragmentInjector, MainNavigator, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnCompleteListener<Location>, VenueAdapter.VenueAdapterListener {

    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;

    @Inject
    GoogleApiClient mGoogleApiClient;

    @Inject
    ViewModelProviderFactory factory;

    @Inject
    LocationCallback mLocationCallback;

    @Inject
    LocationRequest mLocationRequest;

    @Inject
    VenueAdapter mVenueAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    private ActivityMainBinding mActivityMainBinding;
    private MainViewModel mMainViewModel;

    private int currentPage = 0;


    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        mGoogleApiClient.connect();
        setUp();
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public MainViewModel getViewModel() {
        mMainViewModel = ViewModelProviders.of(this, factory).get(MainViewModel.class);
        return mMainViewModel;
    }


    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityMainBinding = getViewDataBinding();
        mVenueAdapter.setListener(this);
    }
    public void onFragmentDetached(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            fragmentManager
                    .beginTransaction()
                    .disallowAddToBackStack()
                    .setCustomAnimations(R.anim.slide_left, R.anim.slide_right)
                    .remove(fragment)
                    .commitNow();
        }
    }


    @SuppressLint("WrongConstant")
    private void setUp() {
        if (!hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestPermissionsSafely(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 99);
        } else {
            mGoogleApiClient.connect();
        }

        mActivityMainBinding.venuesRecyclerView.setAdapter(mVenueAdapter);
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mActivityMainBinding.venuesRecyclerView.setLayoutManager(mLayoutManager);
        mActivityMainBinding.venuesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mActivityMainBinding.venuesRecyclerView.setOnScrollListener(new InfiniteScrollListener(new InfiniteScrollListener.OnEndListener() {
            @Override
            public void onEnd() {
                if (mMainViewModel.mMapLatLng != null) {
                    mMainViewModel.loadVenues(Constants.LIMIT, currentPage * Constants.OFFSET, String.valueOf(mMainViewModel.mMapLatLng.get().getLatitude()) + "," + mMainViewModel.mMapLatLng.get().getLongitude());
                    currentPage++;
                }
            }
        }, mLayoutManager));
        subscribeToLiveData();


    }

    private void subscribeToLiveData() {
        mMainViewModel.getVenueListLiveData().observe(this, venues -> mMainViewModel.setVenueDataList(venues));
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        requestLocation();

    }


    @SuppressLint("MissingPermission")
    public void requestLocation() {
        LocationServices.getFusedLocationProviderClient(this).getLastLocation().addOnCompleteListener(this);
        LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(mLocationRequest, mLocationCallback, null);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        handleError(new Throwable(connectionResult.getErrorMessage()));

    }

    @Override
    public void handleError(Throwable throwable) {
        Toast.makeText(this, throwable.getMessage(), Toast.LENGTH_LONG).show();

    }

    @Override
    public void openDetailFragment(Venue venue,String venueId) {
        getSupportFragmentManager()
                .beginTransaction()
                .disallowAddToBackStack()
                .setCustomAnimations(R.anim.slide_left, R.anim.slide_right)
                .add(R.id.clRootView, DetailFragment.newInstance(venue,venueId), DetailFragment.TAG)
                .commit();
    }

    @Override
    public void updateVenueList(List<Venue> venueList) {
        mVenueAdapter.addItems(venueList);
    }


    @Override
    public void onComplete(@NonNull Task<Location> task) {
        mMainViewModel.setLocation(task.getResult());
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient != null)
            mGoogleApiClient.disconnect();
        if (mLocationCallback != null)
            LocationServices.getFusedLocationProviderClient(this).removeLocationUpdates(mLocationCallback);
    }


    @SuppressLint("DefaultLocale")
    @Override
    public void onRetryClick() {
        try {
            mMainViewModel.loadData(Constants.LIMIT, currentPage * Constants.OFFSET, mMainViewModel.mMapLatLng.get() == null ? "35.710673,51.376590" : String.format("%f,%f", mMainViewModel.mMapLatLng.get().getLatitude(), mMainViewModel.mMapLatLng.get().getLongitude()));
        } catch (Exception e) {
            handleError(new Throwable(e.getMessage()));
        }
    }

    @Override
    public void onItemClick(Venue venue,String venueId) {
        openDetailFragment(venue,venueId);
    }

}
