package org.amin.nearby.ui.main.detail;


import org.amin.nearby.data.DataManager;
import org.amin.nearby.data.models.db.Venue;
import org.amin.nearby.ui.base.BaseViewModel;
import org.amin.nearby.utils.rx.SchedulerProvider;


import java.util.Locale;

import androidx.databinding.ObservableField;

public class DetailViewModel extends BaseViewModel<DetailNavigator> {

    public ObservableField<String> location;

    public final ObservableField<String> address;

    public final ObservableField<String> imageUrl;


    public final ObservableField<String> title;


    public DetailViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        imageUrl = new ObservableField<>();
        title = new ObservableField<>();
        location=new ObservableField<>();
        address = new ObservableField<>();
    }

    public void onNavBackClick() {
        getNavigator().goBack();
    }


    public void initializeWithCurrentData(Venue venue){
        title.set(venue.getName());
        location.set(String.format(Locale.US,"%f , %f",venue.getLocation().getLat(),venue.getLocation().getLng()));
        address.set(venue.getLocation().getAddress());
        getDataFromServer(venue.getId());
    }

    public void refreshWithNewData(Venue venue){
        title.set(venue.getName());
        location.set(String.format(Locale.US,"%f , %f",venue.getLocation().getLat(),venue.getLocation().getLng()));
        address.set(venue.getLocation().getAddress());
        getDataFromServer(venue.getId());
    }

    private void getDataFromServer(String venueId) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .getVenue(venueId)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(venuesResponse -> {
                    if (venuesResponse != null) {
                        refreshWithNewData(venuesResponse);
                        getCompositeDisposable().add(getDataManager().seedVenueDataBase(venuesResponse)
                                .subscribeOn(getSchedulerProvider().io())
                                .subscribe());
                    }
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                }));
    }


}
