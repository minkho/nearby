
package org.amin.nearby.ui.main.detail;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class DetailFragmentProvider {

    @ContributesAndroidInjector
    abstract DetailFragment provideDetailFragmentFactory();
}
