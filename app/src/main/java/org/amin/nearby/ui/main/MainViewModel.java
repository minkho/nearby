package org.amin.nearby.ui.main;

import android.location.Location;
import android.util.Log;

import org.amin.nearby.data.DataManager;
import org.amin.nearby.data.models.db.Venue;
import org.amin.nearby.ui.base.BaseViewModel;
import org.amin.nearby.utils.rx.SchedulerProvider;

import java.util.List;

import androidx.databinding.ObservableArrayList;
import androidx.databinding.ObservableField;
import androidx.databinding.ObservableList;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class MainViewModel extends BaseViewModel {

    public ObservableField<Location> mMapLatLng = new ObservableField<>();
    public MutableLiveData<List<Venue>> venuesLiveData;
    private final ObservableList<Venue> venueDataList = new ObservableArrayList<>();


    public MainViewModel(DataManager dataManager, SchedulerProvider schedulerProvider) {
        super(dataManager, schedulerProvider);
        venuesLiveData = new MutableLiveData<>();
        loadData(10,0,"35.710673,51.376590");
    }

    public void setLocation(Location location) {
        mMapLatLng.set(location);
    }

    public LiveData<List<Venue>> getVenueListLiveData() {
        return venuesLiveData;
    }

    public ObservableList<Venue> getVenueDataList() {
        return venueDataList;
    }

    public void setVenueDataList(List<Venue> venues) {
        venueDataList.clear();
        venueDataList.addAll(venues);
    }

    public void loadData(int limit, int offset, String ll) {
        loadFromDb();
        loadVenues(limit, offset, ll);
    }

    public void loadFromDb() {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .loadFromDb()
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(venuesResponse -> {
                    if (venuesResponse != null) {
                        venuesLiveData.setValue(venuesResponse);
                    }
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                    Log.e("load_venues", throwable.toString());
                }));
    }

    public void loadVenues(int limit, int offset, String ll) {
        setIsLoading(true);
        getCompositeDisposable().add(getDataManager()
                .getVenues(limit, offset, ll)
                .subscribeOn(getSchedulerProvider().io())
                .observeOn(getSchedulerProvider().ui())
                .subscribe(venuesResponse -> {
                    if (venuesResponse != null) {
                        venuesLiveData.setValue(venuesResponse);
                        getCompositeDisposable().add(getDataManager().seedVenueDataBase(venuesResponse)
                                .subscribeOn(getSchedulerProvider().io())
                                .subscribe());
                    }
                    setIsLoading(false);
                }, throwable -> {
                    setIsLoading(false);
                }));
    }

}
