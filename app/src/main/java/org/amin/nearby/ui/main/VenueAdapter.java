package org.amin.nearby.ui.main;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import org.amin.nearby.data.models.db.Venue;
import org.amin.nearby.databinding.ItemVenueEmptyViewBinding;
import org.amin.nearby.databinding.ItemVenueViewBinding;
import org.amin.nearby.ui.base.BaseViewHolder;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class VenueAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    public static final int VIEW_TYPE_EMPTY = 0;

    public static final int VIEW_TYPE_NORMAL = 1;

    private List<Venue> mVenueList;

    private VenueAdapterListener mListener;

    public VenueAdapter(List<Venue> venueList) {
        this.mVenueList = venueList;
    }

    @Override
    public int getItemCount() {
        if (mVenueList != null && mVenueList.size() > 0) {
            return mVenueList.size();
        } else {
            return 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mVenueList != null && !mVenueList.isEmpty()) {
            return VIEW_TYPE_NORMAL;
        } else {
            return VIEW_TYPE_EMPTY;
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        holder.onBind(position);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NORMAL:
                ItemVenueViewBinding venueViewBinding = ItemVenueViewBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new VenueViewHolder(venueViewBinding);
            case VIEW_TYPE_EMPTY:
            default:
                ItemVenueEmptyViewBinding emptyViewBinding = ItemVenueEmptyViewBinding.inflate(LayoutInflater.from(parent.getContext()),
                        parent, false);
                return new EmptyViewHolder(emptyViewBinding);
        }
    }

    public void addItems(List<Venue> venueList) {
        mVenueList.addAll(venueList);
        notifyDataSetChanged();
    }

    public void clearItems() {
        mVenueList.clear();
    }

    public void setListener(VenueAdapterListener listener) {
        this.mListener = listener;
    }

    public interface VenueAdapterListener {

        void onRetryClick();
        void onItemClick(Venue venue,String venueId);
    }

    public class VenueViewHolder extends BaseViewHolder implements VenueItemViewModel.VenueItemViewModelListener {

        private ItemVenueViewBinding mBinding;

        private VenueItemViewModel mVenueItemViewModel;

        public VenueViewHolder(ItemVenueViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            final Venue venue = mVenueList.get(position);
            mVenueItemViewModel = new VenueItemViewModel(venue, this);
            mBinding.setViewModel(mVenueItemViewModel);
            mBinding.executePendingBindings();
        }

        @Override
        public void onItemClick(Venue venue,String venueId) {
            if (venueId != null&&venue!=null) {
                mListener.onItemClick(venue,venueId);
            }
        }
    }

    public class EmptyViewHolder extends BaseViewHolder implements VenueEmptyItemViewModel.VenueEmptyItemViewModelListener {

        private ItemVenueEmptyViewBinding mBinding;

        public EmptyViewHolder(ItemVenueEmptyViewBinding binding) {
            super(binding.getRoot());
            this.mBinding = binding;
        }

        @Override
        public void onBind(int position) {
            VenueEmptyItemViewModel emptyItemViewModel = new VenueEmptyItemViewModel(this);
            mBinding.setViewModel(emptyItemViewModel);
        }

        @Override
        public void onRetryClick() {
            mListener.onRetryClick();
        }
    }
}