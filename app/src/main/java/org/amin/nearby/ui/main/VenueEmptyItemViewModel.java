package org.amin.nearby.ui.main;


public class VenueEmptyItemViewModel {

    private VenueEmptyItemViewModelListener mListener;

    public VenueEmptyItemViewModel(VenueEmptyItemViewModelListener listener) {
        this.mListener = listener;
    }

    public void onRetryClick() {
        mListener.onRetryClick();
    }

    public interface VenueEmptyItemViewModelListener {

        void onRetryClick();
    }
}
