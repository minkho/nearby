package org.amin.nearby.ui.main;

import org.amin.nearby.data.models.db.Venue;

import java.util.List;

public interface MainNavigator {
    void handleError(Throwable throwable);

    void openDetailFragment(Venue venue,String venueId);

    void updateVenueList(List<Venue> venueList);


}
