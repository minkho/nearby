package org.amin.nearby.ui.main;

import org.amin.nearby.data.models.db.Venue;

import java.util.Locale;

import androidx.databinding.ObservableField;

public class VenueItemViewModel {

    public ObservableField<String> location;

    public final ObservableField<String> address;

    public final ObservableField<String> imageUrl;

    public final VenueItemViewModelListener mListener;

    public final ObservableField<String> title;

    private final Venue mVenue;

    public VenueItemViewModel(Venue venue, VenueItemViewModelListener listener) {
        this.mVenue = venue;
        this.mListener = listener;
        imageUrl = new ObservableField<>();
        title = new ObservableField<>(venue.getName());

        location=new ObservableField<>(String.format(Locale.US,"%f , %f",venue.getLocation().getLat(),venue.getLocation().getLng()));

        address = new ObservableField<>(venue.getLocation().getAddress());
    }

    public void onItemClick() {
        mListener.onItemClick(mVenue,mVenue.getId());
    }

    public interface VenueItemViewModelListener {

        void onItemClick(Venue venue,String venueId);
    }
}
