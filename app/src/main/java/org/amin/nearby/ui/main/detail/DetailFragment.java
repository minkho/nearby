package org.amin.nearby.ui.main.detail;

import android.os.Bundle;
import android.view.View;

import org.amin.nearby.BR;
import org.amin.nearby.R;
import org.amin.nearby.ViewModelProviderFactory;
import org.amin.nearby.data.models.db.Venue;
import org.amin.nearby.databinding.FragmentDetailBinding;
import org.amin.nearby.ui.base.BaseFragment;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;


public class DetailFragment extends BaseFragment<FragmentDetailBinding, DetailViewModel> implements DetailNavigator {

    public static final String TAG = DetailFragment.class.getSimpleName();
    @Inject
    ViewModelProviderFactory factory;

    FragmentDetailBinding mFragmentDetailBinding;
    private DetailViewModel mDetailViewModel;
    private String mVenueId;
    private Venue mMinimalVenue;

    public static DetailFragment newInstance(Venue minimalVenue, String venueId) {
        Bundle args = new Bundle();
        DetailFragment fragment = new DetailFragment();
        args.putString("VENUE_ID", venueId);
        args.putSerializable("VENUE", minimalVenue);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_detail;
    }

    @Override
    public DetailViewModel getViewModel() {
        mDetailViewModel = ViewModelProviders.of(this, factory).get(DetailViewModel.class);
        return mDetailViewModel;
    }

    @Override
    public void goBack() {
        getBaseActivity().onFragmentDetached(TAG);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mVenueId = getArguments().getString("VENUE_ID");
            mMinimalVenue = (Venue) getArguments().getSerializable("VENUE");
        }
        mDetailViewModel.setNavigator(this);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mFragmentDetailBinding = getViewDataBinding();
        Initialize();

    }

    private void Initialize() {
        mDetailViewModel.initializeWithCurrentData(mMinimalVenue);
    }
}
