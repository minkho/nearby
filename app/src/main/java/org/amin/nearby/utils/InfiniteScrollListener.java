package org.amin.nearby.utils;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class InfiniteScrollListener extends RecyclerView.OnScrollListener {
    private int previousTotal;
    private boolean loading;
    private int visibleThreshold;
    private final LinearLayoutManager layoutManager;
    private final OnEndListener onEndListener;

    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (dy > 0) {
            int visibleItemCount = recyclerView.getChildCount();
            int totalItemCount = this.layoutManager.getItemCount();
            int firstVisibleItem = this.layoutManager.findFirstVisibleItemPosition();
            if (this.loading && totalItemCount > this.previousTotal) {
                this.loading = false;
                this.previousTotal = totalItemCount;
            }

            if (!this.loading && totalItemCount - visibleItemCount <= firstVisibleItem + this.visibleThreshold) {
                if (onEndListener!=null)
                    onEndListener.onEnd();
                this.loading = true;
            }
        }

    }

    public InfiniteScrollListener(OnEndListener onEndListener, LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
        this.loading = true;
        this.visibleThreshold = 3;
        this.onEndListener=onEndListener;
    }

    public interface OnEndListener {
        void onEnd();
    }
}
