package org.amin.nearby.utils;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import org.amin.nearby.R;
import org.amin.nearby.data.models.db.Venue;
import org.amin.nearby.ui.main.VenueAdapter;

import java.util.List;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;


public final class BindingUtils {

    private BindingUtils() {
    }


    @BindingAdapter({"initMap"})
    public static void initMap(final MapView mapView, final Location latLng) {
        if (mapView != null) {
            mapView.onCreate(new Bundle());
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    googleMap.getUiSettings().setAllGesturesEnabled(false);
                    mapView.onResume();
                    if (latLng != null) {
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latLng.getLatitude(), latLng.getLongitude()), 15));
                        googleMap.addMarker(new MarkerOptions().position(new LatLng(latLng.getLatitude(), latLng.getLongitude())).title("Your location"));
                    }

                }
            });
        }
    }
    @BindingAdapter({"adapter"})
    public static void addBlogItems(RecyclerView recyclerView, List<Venue> venues) {
        VenueAdapter adapter = (VenueAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.addItems(venues);
        }
    }



    @BindingAdapter("imageUrl")
    public static void setImageUrl(ImageView imageView, String url) {
        Context context = imageView.getContext();
        Picasso.with(context).load(url).error(R.drawable.no_image_to_show).placeholder(R.drawable.no_image_to_show).into(imageView);
    }
}
