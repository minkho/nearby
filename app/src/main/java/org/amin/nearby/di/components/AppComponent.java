package org.amin.nearby.di.components;

import android.app.Application;

import org.amin.nearby.NearByApp;
import org.amin.nearby.di.modules.AppModule;
import org.amin.nearby.di.builder.BuildersModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;

@Singleton
@Component(modules = {AndroidInjectionModule.class, BuildersModule.class, AppModule.class})
public interface AppComponent {
    void inject(NearByApp nearByApp);

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}
