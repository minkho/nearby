package org.amin.nearby.di.builder;


import org.amin.nearby.ui.main.MainActivity;
import org.amin.nearby.ui.main.MainActivityModule;
import org.amin.nearby.ui.main.detail.DetailFragmentProvider;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BuildersModule {

    @ContributesAndroidInjector(modules = {MainActivityModule.class, DetailFragmentProvider.class})
    abstract MainActivity bindMainActivity();


}
