package org.amin.nearby.data.remote;

import org.amin.nearby.data.models.api.VenueDetailResponse;
import org.amin.nearby.data.models.api.VenuesResponse;
import io.reactivex.Single;


public interface ApiHelper {
    Single<VenueDetailResponse> venueDetailApiCall(String venueId);

    Single<VenuesResponse> exploreApiCall(int limit, int offset, String ll);
}
