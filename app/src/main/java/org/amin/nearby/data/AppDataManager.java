package org.amin.nearby.data;

import android.content.Context;

import com.google.gson.Gson;

import org.amin.nearby.data.local.db.DbHelper;
import org.amin.nearby.data.models.api.VenueDetailResponse;
import org.amin.nearby.data.models.api.VenuesResponse;
import org.amin.nearby.data.models.db.Venue;
import org.amin.nearby.data.remote.ApiHelper;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Single;


@Singleton
public class AppDataManager implements DataManager {

    private final ApiHelper mApiHelper;

    private final Context mContext;

    private final DbHelper mDbHelper;

    private final Gson mGson;


    @Inject
    public AppDataManager(Context context, DbHelper dbHelper, ApiHelper apiHelper, Gson gson) {
        mContext = context;
        mDbHelper = dbHelper;
        mApiHelper = apiHelper;
        mGson = gson;
    }


    @Override
    public Single<List<Venue>> getVenues(int limit, int offset, String ll) {
        return exploreApiCall(limit, offset, ll).map(venuesResponse -> {
            List<Venue> venueList = new ArrayList<>();
            for (VenuesResponse.Response.Group group : venuesResponse.getResponse().getGroups()) {
                for (VenuesResponse.Response.Group.Item item : group.getItems()) {
                    venueList.add(item.getVenue());
                }

            }
            mDbHelper.saveVenueList(venueList);
            return venueList;
        });

    }

    @Override
    public Single<Venue> getVenue(String venueId) {
        return venueDetailApiCall(venueId).map(venuesResponse -> {
            return venuesResponse.getResponse().getVenue();
        });
    }

    @Override
    public Observable<Boolean> seedVenueDataBase(List<Venue> venueList) {
        return mDbHelper.saveVenueList(venueList);
    }
    @Override
    public Observable<Boolean> seedVenueDataBase(Venue venue) {
        return mDbHelper.saveVenue(venue);
    }

    @Override
    public Single<List<Venue>> loadFromDb() {
        return mDbHelper.getAllVenues().singleOrError();
    }


    @Override
    public Observable<List<Venue>> getAllVenues() {
        return mDbHelper.getAllVenues();
    }

    @Override
    public Observable<List<Venue>> getAllVenuesById(String id) {
        return mDbHelper.getAllVenuesById(id);
    }

    @Override
    public Observable<Boolean> isVenueEmpty() {
        return mDbHelper.isVenueEmpty();
    }

    @Override
    public Observable<Boolean> saveVenue(Venue venue) {
        return mDbHelper.saveVenue(venue);
    }

    @Override
    public Observable<Boolean> saveVenueList(List<Venue> venues) {
        return mDbHelper.saveVenueList(venues);
    }

    @Override
    public Observable<Boolean> removeVenue(Venue venue) {
        return mDbHelper.removeVenue(venue);
    }


    @Override
    public Single<VenueDetailResponse> venueDetailApiCall(final String venueId) {
        return mApiHelper.venueDetailApiCall(venueId);
    }

    @Override
    public Single<VenuesResponse> exploreApiCall(int limit, int offset, String ll) {
        return mApiHelper.exploreApiCall(limit, offset, ll);
    }


}
