package org.amin.nearby.data.local.db;

import org.amin.nearby.data.local.db.dao.VenueDao;
import org.amin.nearby.data.models.db.Venue;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {Venue.class},version = 2)
public abstract class DataBase extends RoomDatabase {
    public abstract VenueDao venueDao();
}
