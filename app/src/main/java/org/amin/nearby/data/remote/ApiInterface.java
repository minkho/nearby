package org.amin.nearby.data.remote;

import org.amin.nearby.data.models.api.VenueDetailResponse;
import org.amin.nearby.data.models.api.VenuesResponse;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("venues/explore")
    Single<VenuesResponse> getVenues(@Query("limit") int limit, @Query("offset") int offset, @Query("ll")String ll);

    @GET("venues/{venue_id}")
    Single<VenueDetailResponse> getVenueDetails(@Path("venue_id")String venueId);

}
