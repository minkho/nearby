package org.amin.nearby.data.remote;

import org.amin.nearby.data.models.api.VenueDetailResponse;
import org.amin.nearby.data.models.api.VenuesResponse;

import javax.inject.Inject;
import javax.inject.Singleton;
import io.reactivex.Single;


@Singleton
public class AppApiHelper implements ApiHelper {
    private final ApiInterface apiInterface;

    @Inject
    public AppApiHelper(ApiInterface apiInterface){
        this.apiInterface=apiInterface;

    }

    @Override
    public Single<VenueDetailResponse> venueDetailApiCall(String venueId) {
        return apiInterface.getVenueDetails(venueId);
    }

    @Override
    public Single<VenuesResponse> exploreApiCall(int limit, int offset, String ll) {
        return apiInterface.getVenues(limit,offset,ll);
    }
}
