package org.amin.nearby.data.models.db;

import android.location.Location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity(tableName = "venues")
public final class Venue implements Serializable {
    public Venue() {

    }
    @PrimaryKey()
    @Expose
    @SerializedName("id")
    @ColumnInfo(name = "id")
    @NonNull
    private String id;


    @Expose
    @SerializedName("name")
    @ColumnInfo(name = "name")
    private String name;


    @Expose
    @Embedded
    @SerializedName("location")
    private Location location;

    @NonNull
    public String getId() {
        return id;
    }

    public void setId(@NonNull String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }


    public static final class Location implements Serializable {
        public Location() {
        }

        @Expose
        @SerializedName("address")
        @ColumnInfo(name = "address")
        private String address;


        @Expose
        @SerializedName("lat")
        @ColumnInfo(name = "lat")
        private Double lat;

        @Expose
        @SerializedName("lng")
        @ColumnInfo(name = "lng")
        private Double lng;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }


        public Double getLat() {
            return lat;
        }

        public void setLat(Double lat) {
            this.lat = lat;
        }

        public Double getLng() {
            return lng;
        }

        public void setLng(Double lon) {
            this.lng = lon;
        }


    }


}
