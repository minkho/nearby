package org.amin.nearby.data.local.db;

import org.amin.nearby.data.models.db.Venue;

import java.util.List;

import io.reactivex.Observable;

public interface DbHelper {
    Observable<List<Venue>> getAllVenues();
    Observable<List<Venue>> getAllVenuesById(String id);
    Observable<Boolean> isVenueEmpty();
    Observable<Boolean>saveVenue(Venue venue);
    Observable<Boolean> saveVenueList(List<Venue> venues);
    Observable<Boolean>removeVenue(Venue venue);

}
