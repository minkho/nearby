package org.amin.nearby.data.local.db;

import org.amin.nearby.data.models.db.Venue;

import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class AppDbHelper implements DbHelper {
    private final DataBase mDataBase;

    @Inject
    public AppDbHelper (DataBase dataBase){
        this.mDataBase=dataBase;
    }

    @Override
    public Observable<List<Venue>> getAllVenues() {
        return Observable.fromCallable(new Callable<List<Venue>>() {
            @Override
            public List<Venue> call() throws Exception {
                return mDataBase.venueDao().loadAll();
            }
        });
    }

    @Override
    public Observable<List<Venue>> getAllVenuesById(final String id) {
        return Observable.fromCallable(new Callable<List<Venue>>() {
            @Override
            public List<Venue> call() throws Exception {
                return mDataBase.venueDao().loadAllById(id);
            }
        });
    }

    @Override
    public Observable<Boolean> isVenueEmpty() {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                return mDataBase.venueDao().loadAll().isEmpty();
            }
        });
    }

    @Override
    public Observable<Boolean> saveVenue(final Venue venue) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                mDataBase.venueDao().insert(venue);
                return true;

            }
        });
    }

    @Override
    public Observable<Boolean> saveVenueList(final List<Venue> venues) {
        return Observable.fromCallable(() -> {
            mDataBase.venueDao().insertAll(venues);
            return true;
        });
    }

    @Override
    public Observable<Boolean> removeVenue(final Venue venue) {
        return Observable.fromCallable(new Callable<Boolean>() {
            @Override
            public Boolean call() throws Exception {
                mDataBase.venueDao().delete(venue);
                return true;
            }
        });
    }
}
