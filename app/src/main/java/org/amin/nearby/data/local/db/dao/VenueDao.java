package org.amin.nearby.data.local.db.dao;

import org.amin.nearby.data.models.db.Venue;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;


@Dao
public interface VenueDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Venue venue);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Venue> venues);

    @Query("SELECT * FROM venues")
    List<Venue> loadAll();

    @Query("SELECT * FROM venues WHERE id =:id")
    List<Venue> loadAllById(String id);

    @Delete
    void delete(Venue user);
}
