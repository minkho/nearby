package org.amin.nearby.data;

import org.amin.nearby.data.local.db.DbHelper;
import org.amin.nearby.data.models.db.Venue;
import org.amin.nearby.data.remote.ApiHelper;

import java.util.List;


import io.reactivex.Observable;
import io.reactivex.Single;


public interface DataManager extends DbHelper, ApiHelper {
    Single<List<Venue>> getVenues(int limit,int offset,String ll);
    Single<Venue> getVenue(String venueId);
    Observable<Boolean> seedVenueDataBase(List<Venue> venueList);

    Observable<Boolean> seedVenueDataBase(Venue venue);

    Single<List<Venue>>loadFromDb();
}
