package org.amin.nearby.data.models.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.amin.nearby.data.models.db.Venue;

import java.util.List;

public class VenuesResponse {
    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    @Expose
    @SerializedName("response")
    private Response response;

    public static class Response {
        public List<Group> getGroups() {
            return groups;
        }

        public void setGroups(List<Group> groups) {
            this.groups = groups;
        }

        @Expose
        @SerializedName("groups")
        private List<Group> groups;

        public static class Group {
            public List<Item> getItems() {
                return items;
            }

            public void setItems(List<Item> items) {
                this.items = items;
            }

            @Expose
            @SerializedName("items")
            private List<Item> items;

            public static class Item {
                @Expose
                @SerializedName("venue")
                private Venue venue;

                public Venue getVenue() {
                    return venue;
                }

                public void setVenue(Venue venue) {
                    this.venue = venue;
                }
            }
        }
    }
}
