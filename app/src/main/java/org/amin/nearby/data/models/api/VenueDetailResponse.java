package org.amin.nearby.data.models.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.amin.nearby.data.models.db.Venue;


public class VenueDetailResponse {
    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    @Expose
    @SerializedName("response")
    private Response response;

    public static class Response {
        @Expose
        @SerializedName("venue")
        private Venue venue;

        public Venue getVenue() {
            return venue;
        }

        public void setVenue(Venue venue) {
            this.venue = venue;
        }
    }
}
